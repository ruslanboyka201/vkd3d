=============================
The vkd3d 3D Graphics Library
=============================

Vkd3d is a 3D graphics library built on top of Vulkan. It has an API very
similar, but not identical, to Direct3D 12.

==============
Building vkd3d
==============

Vkd3d depends on SPIRV-Headers and Vulkan-Headers (>= 1.3.228).

Vkd3d generates some of its headers from IDL files. If you are using the
release tarballs, then these headers are pre-generated and are included. If
you are building from git, then they will be generated at build-time using
widl. By default, vkd3d will use the widl found in `PATH'. If widl is not
available or is not recent (>= 3.21), then you can build Wine with `make
tools/widl/widl' to avoid building all of Wine. You can then point vkd3d's
configure at that widl binary with `WIDL="/path/to/widl"'.

For release builds, you may want to define NDEBUG. If you do not need debug log
messages, you may also consider VKD3D_NO_TRACE_MESSAGES and
VKD3D_NO_DEBUG_MESSAGES. For example, you can pass `CPPFLAGS="-DNDEBUG
-DVKD3D_NO_TRACE_MESSAGES"' to configure.

===========
Using vkd3d
===========

Vkd3d can be used by projects that target Direct3D 12 as a drop-in replacement
at build-time with some modest source modifications.

If vkd3d is available when building Wine, then Wine will use it to support
Direct3D 12 applications.

=====================
Environment variables
=====================

Most of the environment variables used by vkd3d are for debugging purposes. The
environment variables are not considered a part of API and might be changed or
removed in the future versions of vkd3d.

Some of debug variables are lists of elements. Elements must be separated by
commas or semicolons.

 * NO_COLOR - this is an alias of NO_COLOUR.

 * NO_COLOUR - when set, vkd3d-compiler and vkd3d-dxbc will default to
   monochrome output, even when the output supports colour.

 * VKD3D_CONFIG - a list of options that change the behavior of libvkd3d.
    * virtual_heaps - Create descriptors for each D3D12 root signature
      descriptor range instead of entire descriptor heaps. Useful when push
      constant or bound descriptor limits are exceeded.
    * vk_debug - enables Vulkan debug extensions.

 * VKD3D_DEBUG - controls the debug level for log messages produced by
   libvkd3d. Accepts the following values: none, err, fixme, warn, trace.

 * VKD3D_VULKAN_DEVICE - a zero-based device index. Use to force the selected
   Vulkan device.

 * VKD3D_DISABLE_EXTENSIONS - a list of Vulkan extensions that libvkd3d should
   not use even if available.

 * VKD3D_SHADER_CONFIG - a list of options that change the behavior of
   libvkd3d-shader.
    * force_validation - Enable (additional) validation of libvkd3d-shader's
      internal representation of shaders.

 * VKD3D_SHADER_DEBUG - controls the debug level for log messages produced by
   libvkd3d-shader. See VKD3D_DEBUG for accepted values.

 * VKD3D_SHADER_DUMP_PATH - path where shader bytecode is dumped.

 * VKD3D_TEST_DEBUG - enables additional debug messages in tests. Set to 0, 1
   or 2.

 * VKD3D_TEST_FILTER - a filter string. Only the tests whose names matches the
   filter string will be run, e.g. VKD3D_TEST_FILTER=clear_render_target.
   Useful for debugging or developing new tests.

 * VKD3D_TEST_PLATFORM - can be set to "wine", "windows" or "other". The test
   platform controls the behavior of todo(), todo_if(), bug_if() and broken()
   conditions in tests.

 * VKD3D_TEST_SKIP_DXC - when set, tests requiring the dxcompiler library will
   be skipped.

 * VKD3D_TEST_BUG - set to 0 to disable bug_if() conditions in tests.

If the configuration defines 'DXCOMPILER_LIBS=-L/path/to/dxcompiler', Shader
Runner attempts to load libdxcompiler.so or dxcompiler.dll to compile test
shaders in Shader Model 6. LD_LIBRARY_PATH (linux), WINEPATH (wine) or PATH
(native windows) should include the location of dxcompiler if SM 6 shader
tests are desired. If dxcompiler is not found, Shader Runner will compile the
test shaders only in earlier shader models. The DXC source does not contain
code for adding DXBC checksums, so the official release should be installed
from:
https://github.com/microsoft/DirectXShaderCompiler/releases

================
Developing vkd3d
================

Development of vkd3d happens on the Wine GitLab instance
(https://gitlab.winehq.org/wine/vkd3d/). Contributors are encouraged
to submit their patches using the merge request tool.

Each merge request is automatically tested with the GitLab CI
system. See gitlab/README in the Git tree for more details.

============================
Testing with the Agility SDK
============================

Traditionally Microsoft have released the Direct3D 12 development files,
including the debug layer runtime, as part of the larger Windows SDK.
In 2021 the DirectX 12 Agility SDK was introduced, which may be updated
independently of the Windows SDK. If you plan to run the vkd3d
crosstests with Microsoft's debug layer you might want to get it from
the Agility SDK, both because it's probably going to be more up-to-date
and because the Agility SDK is a couple dozens of megabytes versus the
gigabytes of the Windows SDK.

In order to build the vkd3d crosstests with Agility SDK support, follow
these steps:

 * The Agility SDK is distributed at [1]: select your preferred
   version (likely the most recent one) and note the number in column
   D3D12SDKVersion, which you're going to need later.

 [1] https://devblogs.microsoft.com/directx/directx12agility/

 * You also need to enable the "Graphics Tools" optional feature in Windows.
   Open the "Settings" applications, then look for "Apps", "Optional features",
   "View features" and install "Graphics Tools".

 * Configure vkd3d with something like:
   'CROSSCC64="x86_64-w64-mingw32-gcc -DVKD3D_AGILITY_SDK_VERSION=<version>"',
   as well as the equivalent CROSSCC32 variable for the 32-bit
   crosstests. You'll have to replace '<version>' with the
   D3D12SDKVersion number you noted above. Then build the crosstests
   with 'make crosstest' as usual.

 * Download the Agility SDK NuGet package, which is essentially a ZIP
   file with a .nupkg extension. Extract d3d12core.dll and
   d3d12sdklayers.dll for your architecture, and put them in the
   directory containing the crosstest executables.

 * Now you can run the crosstests, possibly with arguments
   '--validate' and '--gbv' to enable the debug layers. They will use
   the runtime from the Agility SDK.

 * It's also possible to use '-DVKD3D_AGILITY_SDK_PATH=/path/to/sdk/' to
   specify the directory to load the Agility SDK DLLs from at runtime.
   If relative, the path is intended to be relative to the executable
   path. If unspecified the path defaults to './'.
